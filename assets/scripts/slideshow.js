let slideIndex = 1
showSlides(slideIndex)

function plusSlides(n) {
    showSlides(slideIndex += n)
}

function currentSlide(n) {
    showSlides(slideIndex = n)
}

function showSlides(n) {
    let target = document.getElementsByClassName("jumbotron")
    if (n > target.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = target.length
    }
    for (let i = 0; i < target.length; i++) {
        target[i].style.display = "none"
    }
    target[slideIndex-1].style.display = "block"
}